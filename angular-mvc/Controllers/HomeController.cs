﻿using angular_mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace angular_mvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getList()
        {
            var emp = new List<Phone>() { new Phone() { name = "phone 1", description = "description 1" }, new Phone() { name = "phone2", description = "description2" } };
            return Json(emp, JsonRequestBehavior.AllowGet);
        }

        public ActionResult list()
        {
            return PartialView();
        }

        public ActionResult search()
        {
            return PartialView();
        }

        public ActionResult router()
        {
            return PartialView();
        }
    }
}
