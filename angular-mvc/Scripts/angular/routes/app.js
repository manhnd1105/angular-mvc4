﻿angular
    .module('myApp', [
    'ngRoute',
    'myControllers',
    ])
    .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

        $routeProvider
            .when('/', {
                //templateUrl: '/Home/list',
                templateUrl: '/Home/router',
                controller: 'PhoneListController'
            })
        .otherwise({ redirectTo: '/Home' });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

    }]);